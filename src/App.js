import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import TentangPraktikum from "./TentangPraktikum/TentangPraktikum";
import OnBoarding from "./OnBoarding/OnBoarding";


function App() {

  return (
    <Router className="OnBoarding">
      <Switch>
        <Route exact path="/">
          <OnBoarding />
        </Route>
        <Route path="/tentangpraktikum">
          <TentangPraktikum />
        </Route>
      </Switch>
    </Router>
  );
}


export default App;
